# Generated by Django 3.2.12 on 2022-07-13 23:28

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('taskstodo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='date_of_creation',
            field=models.DateTimeField(default=datetime.datetime(2022, 7, 13, 23, 28, 46, 816040, tzinfo=utc)),
        ),
    ]
